Rails.application.routes.draw do
  mount Upmin::Engine => '/admin'
  devise_for :users

  root 'welcome#index'

  scope '/tests' do
    get '/' => 'test#index'
    get '/:id' => 'test#show'
    post '/:id/edit' => 'test#update'
    post '/new' => 'test#create'
  end

  scope '/questions' do
    get '/' => 'question#index'
    get '/:id' => 'question#show'
    post '/:id/edit' => 'question#update'
    post '/new' => 'question#create'
  end

  scope '/helps' do
    get '/' => 'help#index'
    get '/:id' => 'help#show'
    post '/:id/edit' => 'help#update'
    post '/new' => 'help#create'
    post '/user/:id' => 'help#by_user'
  end

end
