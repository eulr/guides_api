class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :q
      t.integer :ans
      t.string :a
      t.string :b
      t.string :c
      t.string :d
      t.belongs_to :test, index: true
      t.belongs_to :category, index: true
      t.timestamps null: false
    end
  end
end
