class CreateHelps < ActiveRecord::Migration
  def change
    create_table :helps do |t|
      t.belongs_to :user, index: true
      t.string :text
      t.belongs_to :question, index: true

      t.timestamps null: false
    end
  end
end
