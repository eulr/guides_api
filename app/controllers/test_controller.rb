class TestController < ApplicationController
  # before_action :set_product, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /products
  # GET /products.json
  def index
    @tests = Test.all
    render json: @tests
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @test = Test.find(params[:id])
    render json: @test
  end

  # GET /products/1/edit
  def update
  	@Test = Test.find(params[:id])
  	@Test.name = params[:name]
  	@Test.save
  	render json: @Test
  end

  # POST /products
  def create
    @Test = Test.new(name: params[:name])

	  if @Test.save
	    render json: @Test
	  else
	    render json: @Test.errors, status: :unprocessable_entity
	  end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
  	@test = Test.find(params[:id])
    @test.destroy
    render json: {status: :ok}
  end

end
