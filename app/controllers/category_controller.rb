class CategoryController < ApplicationController
  # before_action :set_product, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /products
  # GET /products.json
  def index
    @categories = Category.all
    render json: @categories
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @category = Category.find(params[:id])
    render json: @category
  end

  # GET /products/1/edit
  def update
  	@category = Category.find(params[:id])
  	@category.name = params[:name]
  	@category.save
  	render json: @category
  end

  # POST /products
  def create
    @category = Category.new(name: params[:name])

	  if @category.save
	    render json: @category
	  else
	    render json: @category.errors, status: :unprocessable_entity
	  end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
  	@category = Category.find(params[:id])
    @category.destroy
    render json: {status: :ok}
  end

end
