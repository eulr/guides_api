class HelpController < ApplicationController
  before_action :set_help, only: [:show, :update, :destroy]
  skip_before_action :verify_authenticity_token

  def index
    @helps = Help.all
    render json: @helps
  end

  def by_user
    @helps = User.find(params[:id]).help
    render json: @helps
  end

  def show
    render json: @helps
  end

  def update
  	@helps.text = params[:text]
  	@helps.save
  	render json: @helps
  end

  def create
    @help = Help.new(text: params[:text], user: current_user, question: Question.find(params[:q]))

	  if @help.save
	    render json: @help
	  else
	    render json: @help.errors, status: :unprocessable_entity
	  end
  end

  def destroy
    @helps.destroy
    render json: {status: :ok}
  end
  
  private

  def set_help
    @helps = Help.find(params[:id])
  end

end
