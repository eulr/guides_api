class QuestionController < ApplicationController
  # before_action :set_product, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  # GET /products
  # GET /products.json
  def index
    @q = Question.all
    render json: @q
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @q = Question.find(params[:id])
    render json: @q
  end

  # GET /products/1/edit
  def update
  	@q = Question.find(params[:id])
  	@q.q = params[:q]
    @q.a = params[:a]
    @q.b = params[:b]
    @q.c = params[:c]
    @q.d = params[:d]
    @q.ans = params[:ans]
    @q.save

  	render json: @q
  end

  # POST /products
  def create
    @q = Question.new
    @q.q = params[:q]
    @q.a = params[:a]
    @q.b = params[:b]
    @q.c = params[:c]
    @q.d = params[:d]
    @q.ans = params[:ans]
    @q.save
    c = Category.find(params[:cat])
    c.question << @q
    t = Test.find(params[:test])
    t.question << @q

	  if @q.save
	    render json: @q
	  else
	    render json: @q.errors, status: :unprocessable_entity
	  end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
  	@q = Question.find(params[:id])
    @q.destroy
    render json: {status: :ok}
  end

end
